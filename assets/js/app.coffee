liveDotaApp = angular.module 'liveDotaApp', ['highcharts-ng', 'ui.bootstrap', 'ui.router',
  'angularMoment', 'wu.masonry', 'jQueryScrollbar']

liveDotaApp.factory 'dota', ['$http', '$interval', ($http, $interval) ->
  matches = {}
  Object.defineProperty matches, '_array',
    enumerable: false
    writable: true
    value: []
  Object.defineProperty matches, '_current_id',
    enumerable: false
    writable: true
    value: null
  Object.defineProperty matches, '_search_id',
    enumerable: false
    writable: true
    value: null
  status =
    timer: 20
  items = {}
  heroes = {}
  leagues = {}
  
  storeMatch = (match) ->
    # Convert players array to hash for easy access
    players = {}
    Object.defineProperty players, '_array',
      enumerable: false
      writable: true
      value: match.players
    for player in players._array
      players[player.account_id] = player
    match.players = players
    
    # Fix match without 'completed' attribute
    if typeof match.completed == 'undefined'
      match.completed = false
    
    if matches[match.match_id]
      angular.merge matches[match.match_id], match
    else
      Object.defineProperty match, '_current_time',
        enumerable: false
        writable: true
        value: 0
      Object.defineProperty match, '_manual_time',
        enumerable: false
        writable: true
        value: false
      Object.defineProperty match, '_info_completed', # This mean we don't need to get new data for this match if marked as true
        enumerable: false
        writable: true
        value: false
      match.started_at = new Date(parseInt(match.id.slice(0,8), 16)*1000)
      matches._array.push match
      matches[match.match_id] = match
      
  updateLiveMatches = ->
    $http.get('/api/v1/live').success (result) ->
      # Store/update match into model
      result.data.forEach (match) ->
        storeMatch match
        
      # Mark latest match as current match
      if !matches._current_id
        current_date = new Date(0)
        for match_id, match of matches
          if match.started_at > current_date
            current_date = match.started_at
            matches._current_id = match_id
        updateMatch matches._current_id
  
  updateMatches = ->
    $http.get('/api/v1/matches?limit=20').success (result) ->
      # Store/update match into model
      result.data.forEach (match) ->
        storeMatch match
  
  updateMatch = (match_id) ->
    if match_id && matches[match_id]
      match = matches[match_id]
      # Ignore information-completed match
      if match._info_completed
        return
      scoreboard = match.scoreboard
      if scoreboard && scoreboard.length > 1
        last_duration =  scoreboard[scoreboard.length - 1].duration
      else
        last_duration = 0
      match._loading_scoreboard = true
      $http.get("/api/v1/matches/#{match_id}/scoreboard?from_duration=#{last_duration}").success (result) ->
        # We only need to get scoreboard once last for completed matches
        if match.completed == true
          match._info_completed = true
        match._loading_scoreboard = false
        
        if scoreboard
          if last_duration == 0
            match.scoreboard = result.data
          else
            result.data.forEach (record) ->
              if record.duration > last_duration
                match.scoreboard.push record
        else
          match.scoreboard = result.data
        
        # When there is scoreboard
        scoreboard = match.scoreboard
        last_record = scoreboard[scoreboard.length - 1]
        team_map = {}
        if last_record
          # Update players information from scoreboard
          last_record.players = last_record.dire.players.concat last_record.radiant.players
          for player in last_record.players
            for orig_player in match.players._array
              if orig_player.account_id == player.account_id
                # Merge attribute
                angular.extend orig_player, player
                # Mark team information
                team_map[player.account_id] = orig_player.team
          
          # Prepare timeline schema
          if !match.timeline
            match.timeline =
              duration: []
              radiant:
                score: []
                tower_state: []
                barracks_state: []
                net_worth: []
              dire:
                score: []
                tower_state: []
                barracks_state: []
                net_worth: []
              players: {}
              events: []
          timeline = match.timeline
          player_attribs = ['level', 'gold', 'kills', 'death', 'assists', 'last_hits', 'denies',
            'gold_per_min', 'xp_per_min', 'net_worth', 'position_y', 'position_x',
            'item0', 'item1', 'item2', 'item3', 'item4', 'item5', 'respawn_timer']
          for player in last_record.players
            if !timeline.players[player.account_id]
              timeline.players[player.account_id] = {}
              for attrib in player_attribs
                timeline.players[player.account_id][attrib] = []
          
          # Update timeline for charting
          for record in result.data
            record.players = record.dire.players.concat record.radiant.players
            # Keep update from scoreboard of duration 0 until the match advances
            if record.duration == 0
              timeline.duration[0]               = record.duration
              timeline.radiant.score[0]          = record.radiant.score
              timeline.radiant.tower_state[0]    = record.radiant.tower_state
              timeline.radiant.barracks_state[0] = record.radiant.barracks_state
              timeline.radiant.net_worth[0]      = 0
              timeline.dire.score[0]             = record.dire.score
              timeline.dire.tower_state[0]       = record.dire.tower_state
              timeline.dire.barracks_state[0]    = record.dire.barracks_state
              timeline.dire.net_worth[0]         = 0
              for player in record.players
                for attrib in player_attribs
                  timeline.players[player.account_id][attrib][0] = player[attrib]
                if team_map[player.account_id] == 0
                  timeline.radiant.net_worth[0] += player.net_worth
                else if team_map[player.account_id] == 1
                  timeline.dire.net_worth[0]    += player.net_worth
            # When the match advances, store new scoreboard to the timeline
            else if timeline.duration.indexOf(record.duration) == -1
              timeline.duration.push               record.duration
              timeline.radiant.score.push          record.radiant.score
              timeline.radiant.tower_state.push    record.radiant.tower_state
              timeline.radiant.barracks_state.push record.radiant.barracks_state
              timeline.dire.score.push             record.dire.score
              timeline.dire.tower_state.push       record.dire.tower_state
              timeline.dire.barracks_state.push    record.dire.barracks_state
              
              radiant_net_worth = 0
              dire_net_worth = 0
              for player in record.players
                for attrib in player_attribs
                  timeline.players[player.account_id][attrib].push player[attrib]
                if team_map[player.account_id] == 0
                  radiant_net_worth += player.net_worth
                else if team_map[player.account_id] == 1
                  dire_net_worth    += player.net_worth
              timeline.radiant.net_worth.push radiant_net_worth
              timeline.dire.net_worth.push    dire_net_worth
              
              # Detect events for this scoreboard
              cur_idx = timeline.duration.indexOf(record.duration)
              events = []
              if cur_idx == 0
                timeline.events.push events
              else if cur_idx > 1
                prev_idx = cur_idx - 1
                for account_id, player of timeline.players
                  duration_distance = timeline.duration[cur_idx] - timeline.duration[prev_idx]
                  # Death event
                  if player.death[cur_idx] > player.death[prev_idx]
                    events.push
                      type: 'death'
                      account_id: account_id
                      death: player.death[cur_idx] - player.death[prev_idx]
                    # Bought back rightaway
                    if player.respawn_timer[cur_idx] == 0 &&
                      duration_distance < (player.level[cur_idx] * 4) # respawn_time(level) = level * 4
                        events.push
                          type: 'bought_back'
                          account_id: account_id
                  if player.kills[cur_idx] > player.kills[prev_idx]
                    events.push
                      type: 'kills'
                      account_id: account_id
                      kills: player.kills[cur_idx] - player.kills[prev_idx]
                timeline.events.push events
                    
              
          # Set duration to the current record duration
          if match._manual_time == false
            match._current_time = last_record.duration
        return # End http
    else if match_id && !matches[match_id]
      # Because this match is not in the model yet, we need to get its general information first
      # TODO: Handle non-exist match
      $http.get("/api/v1/matches/#{match_id}/").success (result) ->
        storeMatch result.data
        # Then get it scoreboard later
        updateMatch match_id
  
  # Wrapper for update everything in interval
  update = ->
    updateLiveMatches()
    updateMatches() # For debugging
    updateMatch matches._current_id
    updateMatch matches._current_completed_id
  update()
  # Run update after 20s
  $interval ->
    if status.timer <= 0
      status.timer = 20
    else
      status.timer -= 1
    if status.timer == 1
      update()
  , 1000
  
  # Update items once
  do ->
    $http.get("/api/v1/items?limit=0").success (result) ->
      result.data.forEach (item) ->
        items[item.item_id] = item
  
  # Update heroes once
  do ->
    $http.get("/api/v1/heroes?limit=0").success (result) ->
      result.data.forEach (hero) ->
        heroes[hero.hero_id] = hero
  
  # Update leagues once
  do ->
    $http.get("/api/v1/leagues?limit=0").success (result) ->
      result.data.forEach (league) ->
        leagues[league.league_id] = league
      
      # Set up pagination
      Object.defineProperty leagues, '_current_page',
        enumerable: false
        writable: true
        value: 1
      Object.defineProperty leagues, '_total',
        enumerable: false
        writable: true
        value: result.data.length
      Object.defineProperty leagues, '_items_per_page',
        enumerable: false
        writeable: true
        value: 12
      Object.defineProperty leagues, '_array',
        enumerable: false
        writable: false
        value: result.data
      Object.defineProperty leagues, '_filter_text',
        enumerable: false
        writable: true
        value: ''
  return {
    matches: matches
    heroes: heroes
    items: items
    leagues: leagues
    status: status
    updateMatch: (match_id) ->
      updateMatch match_id
  }
]

liveDotaApp.controller 'appController', ['$scope', '$http', '$interval', '$filter', 'dota', '$state', ($scope, $http, $interval, $filter, dota, $state) ->
  $scope.matches = dota.matches
  $scope.status = dota.status
  $scope.items    = dota.items
  $scope.heroes   = dota.heroes
  $scope.leagues  = dota.leagues
  
  # Assign a match to current indicator
  $scope.show = (match) ->
    $scope.matches._current_id = match.match_id # pre-assign for immediate update
    dota.updateMatch match.match_id
  $scope.showCompleted = (match) ->
    $scope.matches._current_completed_id = match.match_id
    dota.updateMatch match.match_id
  $scope.$on "$stateChangeSuccess", () ->
    $scope.matches._current_completed_id = $state.params.match_id
    dota.updateMatch $state.params.match_id
  $scope.go = ->
    if $scope.matches._search_id
      $state.go 'matches.match', { match_id: $scope.matches._search_id }
      $scope.matches._search_id = null
]

liveDotaApp.filter 'time', () ->
  return (seconds) ->
    seconds = parseInt seconds, 10
    if isNaN seconds
      seconds = 0
    hours   = Math.floor seconds / 3600
    minutes = Math.floor (seconds - (hours * 3600)) / 60
    seconds = seconds - (hours * 3600) - (minutes * 60)

    time = ''
    if hours != 0
      if hours < 10
        time += "0"
      time += hours + "h "
    if minutes != 0
      if minutes < 10
        time += "0"
      time += minutes + "m "
    if seconds < 10
      time += "0"
    time += seconds + "s"
    time

liveDotaApp.filter 'series_type', () ->
  return (series_type_number) ->
    switch series_type_number
      when 0 then 'BO1'
      when 1 then 'BO3'
      when 2 then 'BO5'
      when 3 then 'BO7'
      else 'Unknown type'

liveDotaApp.filter 'completed', () ->
  return (matches, is_completed) ->
    filtered_matches = []
    for own match_id, match of matches
      if match.completed == is_completed
        filtered_matches.push match
    
    filtered_matches
      
liveDotaApp.filter 'last', () ->
  return (array, attrib) ->
    if angular.isArray(array)
      if attrib
        array[array.length - 1][attrib]
      else
        array[array.length - 1]

liveDotaApp.filter 'team', () ->
  return (team_index) ->
    switch team_index
      when 0 then 'Radiant'
      when 1 then 'Dire'
      else 'Unknown team'

liveDotaApp.filter 'timestampFromId', () ->
  return (id) ->
    new Date(parseInt(id.slice(0,8), 16)*1000)

liveDotaApp.directive 'dotaMatchMap', ['$sce', 'dota', '$timeout', ($sce, dota, $timeout)->
  return {
    templateUrl: '/templates/map.html'
    restrict: 'E'
    scope:
      match: '='
    link: (scope, element) ->
      scope.heroes = dota.heroes
      scope.status = {}
      heatmaps = {}
      teams = ["radiant", "dire"]
      # Order following binary representation of tower state and barrack state
      towers = ["ancient-top", "ancient-bottom", "bottom-3", "bottom-2", "bottom-1",
        "middle-3", "middle-2", "middle-1", "top-3", "top-2", "top-1"]
      barracks = ["bottom-ranged", "bottom-melee", "middle-ranged", "middle-melee",
        "top-ranged", "top-melee"]
      for team in teams
        for name in towers
          scope.status["tower-#{team}-#{name}"] =
            type: "tower"
            team: team
            name: name
            destroyed: false
        for name in barracks
          scope.status["barrack-#{team}-#{name}"] =
            type: "barrack"
            team: team
            name: name
            destroyed: false
      
      scope.position = (player) ->
        if scope.match.timeline
          index = scope.match.timeline.duration.indexOf scope.match._current_time
          player_timeline = scope.match.timeline.players[player.account_id]
          if player_timeline
            position_y = player_timeline.position_y[index]
            position_x = player_timeline.position_x[index]
          map_width = 17408
          map_height = 15872
        return {
          top: (parseFloat(position_y) * (-1) + (map_height / 2)) / map_height * 100
          left: (parseFloat(position_x) + (map_width / 2)) / map_width * 100
        }
      
      # Update tower/barrack status
      scope.update = ->
        # Reset status first
        for index, mark of scope.status
          mark.destroyed = false
          
        # Update status from timeline
        if scope.match && scope.match.timeline
          timeline = scope.match.timeline
          timeline_index = scope.match.timeline.duration.indexOf scope.match._current_time
          ["radiant", "dire"].forEach (team) ->
            tower_state = timeline[team].tower_state[timeline_index].toString(2) # Binary representation
            barrack_state = timeline[team].barracks_state[timeline_index].toString(2)
            for name, index in towers
              scope.status["tower-#{team}-#{name}"].destroyed = (tower_state[index] == '0')
            for name, index in barracks
              scope.status["barrack-#{team}-#{name}"].destroyed = (barrack_state[index] == '0')
        return
        
      scope.dominance = ->
        dominance =
          radiant: 0
          dire: 0
        if scope.match && scope.match.timeline
          index = scope.match.timeline.duration.indexOf scope.match._current_time
          # Projected positions to the diagonal line
          radiant_positions = []
          dire_positions = []
          diagonal_width = 141.4
          map_width = 17408
          map_height = 15872
          for player in scope.match.players._array
            unless player.team == 0 || player.team == 1
              continue
            player_timeline = scope.match.timeline.players[player.account_id]
            diagonals = []
            for i in [0..index]
              x = (parseFloat(player_timeline.position_x[i]) + (map_width / 2)) / map_width * 100
              y = (parseFloat(player_timeline.position_y[i]) * (-1) + (map_height / 2)) / map_height * 100
              diagonals.push x * Math.cos(45) + (100 - y) * Math.sin(45) # Rotate coordinate 45 degree counter-clockwise
            position_diagonal = diagonals.reduce ((p,c,i) -> p+(c-p)/(i+1)), 0
            if player.team == 0
              radiant_positions.push position_diagonal
            else if player.team == 1
              dire_positions.push position_diagonal
          dominance.radiant = radiant_positions.reduce(((p,c,i) -> p+(c-p)/(i+1)), 0) / diagonal_width * 100
          dominance.dire = (diagonal_width - dire_positions.reduce(((p,c,i) -> p+(c-p)/(i+1)), 0)) / diagonal_width * 100
        
        dominance
      
      # Create heatmap on demand
      generateMap = (player) ->
        $map = element.find('.map-background')
        $wrapper = element.find('.map-wrapper')
        map_bg_width = $map.width()
        map_bg_height = $map.height()
        map_width = 17408
        map_height = 15872
        index = scope.match.timeline.duration.indexOf scope.match._current_time
        player_timeline = scope.match.timeline.players[player.account_id]
        
        if player._heatmap && # Check for existing map
          player._heatmap.canvas_width == map_bg_width &&  # Re-render map if window resize
          player._heatmap.last_index <= index # Re-render map if user is rolling back in time
            heatmap = player._heatmap
            last_index = heatmap.last_index
        else
          # Destroy old canvas if existed
          if player._heatmap
            angular.element(player._heatmap._renderer.canvas).remove()
          player._heatmap = h337.create
            container: $wrapper[0]
          heatmap = player._heatmap
          heatmap.canvas_width = map_bg_width
          heatmap._store._max = 5
          heatmap._store._min = 0
          last_index = -1
        
        # We will need to scale the position of range [0..map_width]x[0..map_height] to [0..map_bg_width]x[0..map_bg_height]
        for i in [(last_index+1)..index]
          x = (parseFloat(player_timeline.position_x[i]) + (map_width / 2)) / map_width * map_bg_width
          y = (parseFloat(player_timeline.position_y[i]) * (-1) + (map_height / 2)) / map_height * map_bg_height
          heatmap.addData
            x: x
            y: y
            value: 1
        # Store last index to avoid the need to recreate dataset
        last_index = index
        
        # Return the map
        heatmap
      scope.showMap = (player) ->
        if scope.match.timeline
          heatmap = generateMap player
          angular.element(heatmap._renderer.canvas).show()
        return
      scope.hideMap = (player) ->
        if player._heatmap
          angular.element(player._heatmap._renderer.canvas).hide()
        return
      
      scope.$watch ->
        scope.match._current_time if scope.match
      , scope.update
      scope.$watch ->
        scope.match.timeline if scope.match
      , scope.update
      return
  }
]

liveDotaApp.directive 'dotaMatchChart', ['$window', '$filter', '$timeout', '$interval', 'dota', ($window, $filter, $timeout, $interval, dota) ->
  return {
    templateUrl: '/templates/chart.html'
    restrict: 'E'
    scope:
      match: '='
    link: (scope) ->
      scope.status = dota.status
      scope.chart =
        type: 'gold'
        data:
          options:
            chart:
              type: 'line'
              events:
                click: (e) ->
                  scope.$evalAsync ->
                    # Update duration from stored hovered point duration
                    setTime scope.chart.current_hovered_point_x
              marginRight: 20
            title: false
            plotOptions:
              series:
                animation: false
                point:
                  events:
                    click: (e) ->
                      that = this
                      scope.$evalAsync ->
                        setTime that.x
                    mouseOver: (e) ->
                      # Store the duration value of current hovered point
                      scope.chart.current_hovered_point_x = this.x
              line:
                marker:
                  enabled: false
            tooltip:
              useHTML: true
              formatter: ->
                return "<div class='text-center'>#{$filter('time')(this.x)}</div>
                  <span style=\"color:#{this.point.color}\">\u25CF</span> #{this.series.name}: <b>#{this.point.y}</b>"
          series: []
          xAxis:
            type: 'linear'
            labels:
              formatter: ->
                return $filter('time')(this.value)
            plotLines: [
              color: 'blue'
              label:
                text: 'Current time'
                rotation: 0
                align: 'right'
                x: -5
              value: -1
              width: 3
              zIndex: 10
            ]
              
          yAxis:
            title:
              text: 'Gold'
      
      # Generate chart data from match data
      scope.update = ->
        if !scope.match
          return
        if !scope.match.timeline
          # Wait for timeline
          scope.chart.data.series = [] # Reset timeline for non-ready match
          $timeout scope.update, 10
          return
        
        timeline = scope.match.timeline
        switch scope.chart.type
          when 'gold'
            scope.chart.data.series = [{
              name: 'Radiant'
              color: 'green'
            }, {
              name: 'Dire'
              color: 'red'
            }]
            scope.chart.data.series[0].data = timeline.duration.map (second, index) ->
              [second, timeline.radiant.net_worth[index]]
              
            scope.chart.data.series[1].data = timeline.duration.map (second, index) ->
              [second, timeline.dire.net_worth[index]]
            
            scope.chart.data.yAxis.title.text = 'Gold'
          when 'gold_diff'
            scope.chart.data.series = [{
              name: 'Radiant / Dire'
              color: 'transparent'
              type: 'area'
              fillColor: 'green'
              negativeFillColor: 'red'
            }]
            
            scope.chart.data.series[0].data = timeline.duration.map (second, index) ->
              [second, timeline.radiant.net_worth[index] - timeline.dire.net_worth[index]]
                
            scope.chart.data.yAxis.title.text = 'Gold'
          when 'exp'
            scope.chart.data.series = [{
              name: 'Radiant'
              color: 'green'
            }, {
              name: 'Dire'
              color: 'red'
            }]
            scope.chart.data.series[0].data = timeline.duration.map (second, index) ->
              exp = 0
              for account_id, player of scope.match.players
                if player.team == 0
                  exp += timeline.players[account_id].xp_per_min[index] / 60 * second
              [second, exp]
              
            scope.chart.data.series[1].data = timeline.duration.map (second, index) ->
              exp = 0
              for account_id, player of scope.match.players
                if player.team == 1
                  exp += timeline.players[account_id].xp_per_min[index] / 60 * second
              [second, exp]
            
            scope.chart.data.yAxis.title.text = 'Experience'
          when 'exp_diff'
              scope.chart.data.series = [{
                name: 'Radiant / Dire'
                color: 'transparent'
                type: 'area'
                fillColor: 'green'
                negativeFillColor: 'red'
              }]
              
              scope.chart.data.series[0].data = timeline.duration.map (second, index) ->
                radiant_exp = 0
                dire_exp = 0
                for account_id, player of scope.match.players
                  if player.team == 0
                    radiant_exp += timeline.players[account_id].xp_per_min[index] / 60 * second
                  else if player.team == 1
                    dire_exp += timeline.players[account_id].xp_per_min[index] / 60 * second
                [second, radiant_exp - dire_exp]
                
              scope.chart.data.yAxis.title.text = 'Experience'
        
        # Update plot line position
        # if scope.chart.data.xAxis.plotLines[0].value < 0 || timeline.duration.indexOf(scope.chart.data.xAxis.plotLines[0].value) == -1
        #   setTime timeline.duration[timeline.duration.length - 1], true
        # return
      
      # Expose selected time
      setTime = (time, skip_feedback) ->
        # Update plot line position
        line = scope.chart.data.xAxis.plotLines[0]
        line.value = time
        line.label.text = "#{$filter('time')(line.value)}"
        
        # Feed duration back to service
        if !skip_feedback
          scope.match._current_time = time
          scope.match._manual_time = true
      scope.setTime = setTime
      
      # Change timeline back to auto
      scope.resetTime = ->
        scope.match._current_time = scope.match.duration
        scope.match._manual_time = false
        
      # Watch for scoreboard and chart type changes then update the chart
      scope.$watch ->
        if scope.match
          scope.match.scoreboard
        else
          scope.match
      , scope.update
      , true # Object equality
      scope.$watch ->
        scope.chart.type
      , scope.update
      
      # Watch for current duration and update the plot line
      scope.$watch ->
        if scope.match
          scope.match._current_time
      , ->
        if scope.match
          scope.$evalAsync ->
            setTime scope.match._current_time, true
      
      # Auto resize chart height to match map height
      chart_resize = ->
        map_height = angular.element('dota-match-map').height()
        heading_height = angular.element('dota-match-chart .panel-heading').height()
        footer_height = angular.element('dota-match-chart .panel-footer').height()
        chart_margin = 72
        chart_height = map_height - (heading_height + footer_height + chart_margin)
        scope.chart.data.options.chart.height = chart_height
      angular.element($window).on 'resize', ->
        scope.$evalAsync chart_resize
      wait_for_loaded = $interval ->
        # Wait for map image load
        if angular.element('dota-match-map img').height() > 0
          chart_resize()
          $interval.cancel wait_for_loaded
      , 10
  }
]

liveDotaApp.directive 'dotaMatchInfo', ['$filter', 'dota', ($filter, dota)->
  return {
    templateUrl: '/templates/info.html'
    restrict: 'E'
    scope:
      match: '='
    link: (scope) ->
      scope.leagues = dota.leagues
      
      scope.getStatus = ->
        match = scope.match
        status = "Waiting for game to start"
        if match.completed == true
          status = "Completed"
        else if match.duration == 0
          for player in match.players
            if (player.team == 0 || player.team == 1) && player.hero_id == 0
              status = "Picking phase"
        else if match.duration > 0
          if match.paused
            status = "Paused"
          else
            status = "Live #{$filter('time')(match.duration)}"
        
        status
      
      scope.getNetWorthAdv = ->
        match = scope.match
        if match.timeline
          length = match.timeline.duration.length
          adv = match.timeline.radiant.net_worth[length - 1] - match.timeline.dire.net_worth[length - 1]
          if adv >= 0
            "Radiant by #{adv}"
          else
            "Dire by #{adv * -1}"
  }
]

liveDotaApp.directive 'dotaMatchLog', ['dota', (dota) ->
  return {
    templateUrl: '/templates/log.html'
    restrict: 'E'
    scope:
      match: '='
    link: (scope) ->
  }
]

liveDotaApp.directive "dotaMatchPlayers", ['dota', (dota) ->
  return {
    templateUrl: '/templates/players.html'
    restrict: 'E'
    scope:
      match: '='
    link: (scope) ->
      scope.heroes = dota.heroes
      scope.items  = dota.items
      scope.get = (player, attrib) ->
        if scope.match.timeline && scope.match.timeline.players[player.account_id]
          index = scope.match.timeline.duration.indexOf scope.match._current_time
          scope.match.timeline.players[player.account_id][attrib][index]
      # Proxy to MatchMap scope
      scope.showMap = (player) ->
        map_scope = angular.element('dota-match-map').children().scope()
        map_scope.showMap(player)
        return
      scope.hideMap = (player) ->
        map_scope = angular.element('dota-match-map').children().scope()
        map_scope.hideMap(player)
        return
  }
]

liveDotaApp.directive 'errSrc', ->
  return {
    link: (scope, element, attrs) ->
      element.bind 'error', ->
        if attrs.src != attrs.errSrc
          attrs.$set 'src', attrs.errSrc
  }

liveDotaApp.directive 'a', ->
  return {
    restrict: 'E'
    link: (scope, elem, attrs) ->
      if attrs.ngClick || attrs.href == '' || attrs.href == '#'
        elem.on 'click', (e) ->
          e.preventDefault()
  }

liveDotaApp.config ($stateProvider, $urlRouterProvider, $locationProvider) ->
  $urlRouterProvider.otherwise('/live')
  $locationProvider.hashPrefix('!')
  
  $stateProvider
    .state 'live',
      url: '/live'
      templateUrl: 'templates/live/index.html'
    .state 'leagues',
      url: '/leagues'
      templateUrl: 'templates/leagues/index.html'
    .state 'matches',
      url: '/matches'
      templateUrl: 'templates/matches/index.html'
    .state 'matches.match',
      url: '/:match_id'
      templateUrl: 'templates/matches/index.html'
      controller: 'appController'
