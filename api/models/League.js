/**
* Leauge.js
*
* @description :: Access Dota 2 leagues from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'leagues'
};
