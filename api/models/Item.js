/**
* Item.js
*
* @description :: Access Dota 2 items from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'items'
};
