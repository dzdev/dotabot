/**
* Player.js
*
* @description :: Access Dota 2 players from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'players'
};
