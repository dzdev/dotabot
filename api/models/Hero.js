/**
* Hero.js
*
* @description :: Access Dota 2 heroes from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'heroes'
};
