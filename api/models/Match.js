/**
* Match.js
*
* @description :: Access Dota 2 matches from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'matches'
};
