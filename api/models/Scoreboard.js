/**
* Scoreboard.js
*
* @description :: Access Dota 2 scoreboard from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'scoreboard'
};
