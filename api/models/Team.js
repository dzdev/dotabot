/**
* Team.js
*
* @description :: Access Dota 2 teams from database
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'teams'
};
