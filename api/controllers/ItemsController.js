/**
 * ItemsController
 *
 * @description :: Server-side logic for managing items
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    _config: {
        actions: false,
        rest: false,
        shortcut: false
    },
    /**
     * List items
     */
    index: function (req, res) {
        var page = req.query.page || 1,
            limit = req.query.limit || 10,
            ids = req.query.ids,
            filter;
        if (ids && ids.length) {
            filter = {
                item_id: ids.split(',').map(function(id) {return parseInt(id)})
            };
        } else {
            filter = {};
        }
        
        Item.find(filter).paginate({ page: page, limit: limit }).exec(function(err, result) {
            if (err) {
                return res.serverError(err);
            }
            return res.json({
                status: 'ok',
                data: result
            })
        });
    },
    /**
     * Show player information
     * @param int account_id
     */
    show: function (req, res) {
        Item.find({ item_id: parseInt(req.param('item_id'))}).limit(1).exec(function(err, result) {
            if (err) {
                return res.serverError(err);
            }
            if (result.length == 0) {
                return res.notFound();
            }
            
            return res.json({
                status: 'ok',
                data: result[0]
            });
        });
    }
}
