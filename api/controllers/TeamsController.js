/**
 * TeamsController
 *
 * @description :: Server-side logic for managing teams
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    _config: {
        actions: false,
        rest: false,
        shortcut: false
    },
    /**
     * Show team list
     * @param string ids - Comma-separated list of teams' ids
     */
    index: function (req, res) {
        var page = req.query.page || 1,
            limit = req.query.limit || 10,
            ids = req.query.ids,
            filter;
        if (ids && ids.length) {
            filter = {
                team_id: ids.split(',').map(function(id) {return parseInt(id)})
            };
        } else {
            filter = {};
        }
        
        Team.find(filter).paginate({ page: page, limit: limit }).exec(function(err, result) {
            if (err) {
                return res.serverError(err);
            }
            return res.json({
                status: 'ok',
                data: result
            })
        });
    },
    /**
     * Show team information
     * @param int team_id
     */
    show: function (req, res) {
        Team.find({ team_id: parseInt(req.param('team_id'))}).limit(1).exec(function(err, result) {
            if (err) {
                return res.serverError(err);
            }
            if (result.length == 0) {
                return res.notFound();
            }
            
            return res.json({
                status: 'ok',
                data: result[0]
            });
        });
    }
}
