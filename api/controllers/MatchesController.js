/**
 * MatchesController
 *
 * @description :: Server-side logic for managing matches
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    _config: {
        actions: false,
        rest: false,
        shortcut: false
    },
    /**
     * Show live matches
     */
    live: function(req, res) {
        Match.find({ completed: { 'not': true }}).exec(function(err, matches) {
            if (err) {
                return res.serverError(err);
            }
            
            return res.json({
                status: 'ok',
                data: matches
            });
        });
    },
    /**
     * Show old matches
     */
    index: function(req, res) {
        var page = req.query.page || 1,
            limit = req.query.limit || 10,
            ids = req.query.ids,
            filter;
        if (ids && ids.length) {
            filter = {
                match_id: ids.split(',').map(function(id) {return parseInt(id)}),
                completed: true
            };
        } else {
            filter = {
                completed: true
            };
        }
        
        Match.find(filter).sort('_id desc').paginate({ page: page, limit: limit }).exec(function(err, matches) {
            if (err) {
                return res.serverError(err);
            }
            
            return res.json({
                status: 'ok',
                data: matches
            });
        });
    },
    /**
     * Show a specific matches
     */
    show: function(req, res) {
        Match.find({ where: { match_id: parseInt(req.param('match_id')) }, limit: 1}, function(err, matches) {
            if (err) {
                return res.serverError(err);
            }
            
            if (matches.length == 1) {
                return res.json({
                    status: 'ok',
                    data: matches[0]
                })
            } else {
                return res.notFound();
            }
        });
    },
    /**
     * Show scoreboard for a matches
     */
    scoreboard: function(req, res) {
        var filter = {
            match_id: parseInt(req.param('match_id')),
            duration: {
                '>=': (req.query.from_duration) ? parseInt(req.query.from_duration) : 0
            }
        }
        Scoreboard.find(filter).exec(function(err, scoreboard) {
            if (err) {
                return res.serverError(err);
            }
                        
            return res.json({
                status: 'ok',
                data: scoreboard
            })
        })
    }
};
