/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/api/v1/live':                 'MatchesController.live',
  '/api/v1/matches':              'MatchesController.index',
  '/api/v1/matches/:match_id':    'MatchesController.show',
  '/api/v1/matches/:match_id/scoreboard': 'MatchesController.scoreboard',
  '/api/v1/players':              'PlayersController.index',
  '/api/v1/players/:account_id':  'PlayersController.show',
  '/api/v1/heroes':               'HeroesController.index',
  '/api/v1/heroes/:hero_id':      'HeroesController.show',
  '/api/v1/items':                'ItemsController.index',
  '/api/v1/items/:item_id':       'ItemsController.show',
  '/api/v1/teams':                'TeamsController.index',
  '/api/v1/teams/:team_id':       'TeamsController.show',
  '/api/v1/leagues':              'LeaguesController.index',
  '/api/v1/leagues/:league_id':   'LeaguesController.show'
  
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
