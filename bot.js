require('coffee-script/register');

var DotaBot = require(__dirname + '/lib/dotabot'),
    path = require('path'),
    net = require('net'),
    fs = require('fs'),
    LEAGUES_UPDATE_INTERVAL         = 60 * 60 * 1000,
    LIVE_GAMES_UPDATE_INTERVAL      = 15 * 1000,
    HEROES_UPDATE_INTERVAL          = 24 * 60 * 60 * 1000,
    ITEMS_UPDATE_INTERVAL           = 24 * 60 * 60 * 1000,
    IMAGES_UPDATE_INTERVAL          = 30 * 1000,
    COMPLETED_GAMES_UPDATE_INTERVAL = 15 * 60 * 1000,
    SCHEMA_UPDATE_INTERVAL          = 24 * 60 * 60 * 1000,
    LEAGUE_IMAGES_UPDATE_INTERVAL   = 24 * 60 * 60 * 1000,
    root = path.resolve(__dirname + '/.tmp/public/images/dota');

// Set up Bot parameters
DotaBot.key    = process.env.STEAM_API_KEY;
DotaBot.db_uri = process.env.DOTABOT_DB_URI;

// Update leagues loop
var fetch_leagues = function(only_once) {
  DotaBot.updateLeagues(function(err, league_result) {
    if (err) {
      console.error('%s %s Failed to update leagues: %s', log_time(), '[ERROR]', err.message);
    } else {
      console.info('%s %s Added %d league(s), updated %d league(s)', log_time(), '[INFO]', league_result.nUpserted, league_result.nModified);
    }
    if (!only_once) {
      setTimeout(fetch_leagues, LEAGUES_UPDATE_INTERVAL);
    }
  });
};

// Update schema
var fetch_schema = function(only_once) {
  DotaBot.updateSchema(function(err, result) {
    if (err) {
      console.error('%s %s Failed to update schema: %s', log_time(), '[ERROR]', err.message);
    } else {
      console.info('%s %s Updated schema', log_time(), '[INFO]');
      fetch_league_images(true);
    }
    if (!only_once) {
      setTimeout(fetch_schema, SCHEMA_UPDATE_INTERVAL);
    }
  });
}

// Update league logos
var fetch_league_images = function(only_once, im_check) {
  DotaBot.db.leagues.find({},{ league_id: 1, itemdef: 1 }).sort({ '_id': -1 }).toArray(function(err, leagues) {
    if (err) {
      console.error('%s %s Failed to get league logos: %s', log_time(), '[ERROR]', err.message);
    } else {
      var processed_count = 0;
      leagues.forEach(function(league) {
        var file_path = root + '/leagues/league_' + league.league_id + '.png';
        DotaBot.downloadFileByItemDef({ item_id: league.itemdef, file_path: file_path, im_check: im_check }, function (err, result) {
          if (err) {
            console.warn('%s %s Failed to get league %d logo: %s', log_time(), '[WARN]', league.league_id, err.message);
          }
          if (result) {
            console.info('%s %s Downloaded league %d logo', log_time(), '[INFO]', league.league_id);
          }
          processed_count += 1;
          if (processed_count >= leagues.count && !only_once) {
            setTimeout(fetch_league_images, LEAGUE_IMAGES_UPDATE_INTERVAL);
          }
        });
      });
    }
  });
}

// Update live games
var fetch_live_games = function(only_once) {
  DotaBot.updateLiveLeagueGames(function(err, match_result, scoreboard_result) {
    if (err) {
      console.error('%s %s Failed to update live league games: %s', log_time(), '[ERROR]', err.message);
    } else {
      if (match_result) {
        if (scoreboard_result) {
          console.info('%s %s Added %d match(es), updated %d match(es), added %d scoreboard(s), updated %d scoreboard(s)',
            log_time(), '[INFO]', match_result.nUpserted, match_result.nModified, scoreboard_result.nUpserted, scoreboard_result.nModified);
        } else {
          console.info('%s %s Added %d match(es), updated %d match(es)',
            log_time(), '[INFO]', match_result.nUpserted, match_result.nModified);
        }
        

        // When new matches arrive
        if (match_result.nUpserted > 0) {
          // Update teams
          DotaBot.updateTeams(function(err, teams_result) {
            if (err) {
              console.error('%s %s Failed to update teams: %s', log_time(), '[ERROR]', err.message);
            } else {
              console.info('%s %s Added %d team(s)', log_time(), '[INFO]', teams_result.nUpserted);
            }
            // Update logos
            fetch_images(true);
          });
          
          
          // Update players
          DotaBot.updatePlayers(function(err, players_result) {
            if (err) {
              console.error('%s %s Failed to update players: %s', log_time(), '[ERROR]', err.message);
            } else {
              console.info('%s %s Added %d player(s)', log_time(), '[INFO]', players_result.result.n);
            }
            // Update avatars
            fetch_images(true);
          });
          
          // Update leagues
          fetch_leagues(true);
        }
      }
    }
    
    // Chain with update completed games
    fetch_completed_games(true);
    
    // Continue loop
    if (!only_once) {
      setTimeout(fetch_live_games, LIVE_GAMES_UPDATE_INTERVAL);
    }
  })
};

// Update completed games
var fetch_completed_games = function(only_once) {
  DotaBot.updateCompletedMatches(function(err, result) {
    if (err) {
      console.error('%s %s Failed to update matches: %s', log_time(), '[ERROR]', err.message);
      if (result && result.nModified > 0) {
        console.info('%s %s Updated %d completed match(es)', log_time(), '[INFO]', result.nModified);
      }
    } else if (result && result.nModified > 0) {
      console.info('%s %s Updated %d completed match(es)', log_time(), '[INFO]', result.nModified);
    }
    
    if (!only_once) {
      setTimeout(fetch_completed_games, COMPLETED_GAMES_UPDATE_INTERVAL);
    }
  })
}

// Update heroes
var fetch_heroes = function(only_once) {
  DotaBot.updateHeroes(function (err, heroes_result) {
    if (err) {
      console.error('%s %s Failed to update heroes %s', log_time(), '[ERROR]', err.message);
    } else {
      console.info('%s %s Added %d hero(es), updated %d hero(es)', log_time(), '[INFO]', heroes_result.nUpserted, heroes_result.nModified);
      fetch_hero_images(true);
    }
    
    // Continue loop
    if (!only_once) {
      setTimeout(fetch_heroes, HEROES_UPDATE_INTERVAL);
    }
  });
};

// Update items
var fetch_items = function(only_once) {
  DotaBot.updateItems(function (err, items_result) {
    if (err) {
      console.error('%s %s Failed to update items %s', log_time(), '[ERROR]', err.message);
    } else {
      console.info('%s %s Added %d item(s), updated %d item(s)', log_time(), '[INFO]', items_result.nUpserted, items_result.nModified);
      fetch_item_images(true);
    }
    
    // Continue loop
    if (!only_once) {
      setTimeout(fetch_items, ITEMS_UPDATE_INTERVAL);
    }
  });
};

// Download images
var fetch_images = function(only_once, im_check) {
  // Teams logo
  DotaBot.db.teams.find({}, {team_id: 1, logo: 1, logo_sponsor: 1}).sort({'_id': -1}).toArray(function (err, data) {
    if (err) {
      return console.error('%s %s Failed to download team logos: %s', log_time(), '[ERROR]', err.message);
    }
    
    data.forEach(function(team) {
      if (team.logo) {
        DotaBot.downloadFileByID({
          file_id: team.logo,
          file_path: root + '/teams/team_' + team.team_id + '.png',
          im_check: im_check
        }, function(err, result) {
          if (err) {
            console.warn('%s %s Failed to download team logo %s: %s', log_time(), '[WARN]', team.team_id, err.message);
          }
          if (result) {
            console.info('%s %s Downloaded team logo %s', log_time(), '[INFO]', team.team_id);
          }
        });
      }
      if (team.logo_sponsor) {
        DotaBot.downloadFileByID({
          file_id: team.logo_sponsor,
          file_path: root + '/teams/team_' + team.team_id + '_sponsor.png',
          im_check: im_check
        }, function(err, result) {
          if (err) {
            console.warn('%s %s Failed to download team logo sponsor %s: %s', log_time(), '[WARN]', team.team_id, err.message);
          }
          if (result) {
            console.info('%s %s Downloaded team logo sponsor %s', log_time(), '[INFO]', team.team_id);
          }
        });
      }
    });
  });
  
  // Player avatars
  DotaBot.db.players.find({}, {account_id: 1, avatar: 1, avatarmedium: 1, avatarfull: 1}).sort({'_id': -1}).toArray(function(err, data) {
    if (err) {
      return console.error('%s %s Failed to download player images: %s', log_time(), '[ERROR]', err.message)
    }
    
    data.sort({'_id': -1}).forEach(function(player) {
      [['avatar', 'small'], ['avatarmedium', 'medium'], ['avatarfull', 'full']].forEach(function(size) {
        var file_name = 'player_' + player.account_id + '_' + size[1] + '.jpg';
        DotaBot.downloadFileByURL({
          file_url: player[size[0]],
          file_path: root + '/players/' + file_name,
          im_check: im_check
        }, function(err, result) {
          if (err) {
            console.warn('%s %s Failed to download player avatar %s: %s', log_time(), '[WARN]', file_name, err.message);
          }
          if (result) {
            console.info('%s %s Downloaded player avatar %s', log_time(), '[INFO]', file_name);
          }
        });
      });
    });
  });
  
  if (!only_once) {
    setTimeout(fetch_images, IMAGES_UPDATE_INTERVAL);
  }
};

var fetch_hero_images = function(only_once, im_check) {
  // Heroes image
  DotaBot.db.heroes.distinct('name', function(err, data) {
    if (err) {
      return console.error('%s %s Failed to download hero images: %s', log_time(), '[ERROR]', err.message);
    }
    
    data.forEach(function(hero_name) {
      [['hphover', 'png'], ['sb', 'png'], ['full', 'png'], ['vert', 'jpg']].forEach(function(size) {
        var file_name = hero_name + '_' + size[0] + '.' + size[1];
        DotaBot.downloadFileByURL({
          file_url: 'http://media.steampowered.com/apps/dota2/images/heroes/' + file_name,
          file_path: root + '/heroes/' + file_name,
          im_check: im_check
        }, function(err, result) {
          if (err) {
            console.warn('%s %s Failed to download hero image %s: %s', log_time(), '[WARN]', file_name, err.message);
          }
          if (result) {
            console.info('%s %s Downloaded hero image %s', log_time(), '[INFO]', file_name);
          }
        });
      });
    });
  });
  
  if (!only_once) {
    setTimeout(fetch_images, IMAGES_UPDATE_INTERVAL);
  }
}

var fetch_item_images = function(only_once, im_check) {
  // Item images
  DotaBot.db.items.distinct('name', function(err, data) {
    if (err) {
      return console.error('%s %s Failed to download item images: %s', log_time(), '[ERROR]', err.message);
    }
    
    data.forEach(function(item_name) {
      var file_name = item_name + '_lg.png';
      DotaBot.downloadFileByURL({
        file_url: 'http://media.steampowered.com/apps/dota2/images/items/' + file_name,
        file_path: root + '/items/' + file_name,
        im_check: im_check
      }, function(err, result) {
        if (err) {
          console.warn('%s %s Failed to download item image %s: %s', log_time(), '[WARN]', file_name, err.message);
        }
        if (result) {
          console.info('%s %s Downloaded item image %s', log_time(), '[INFO]', file_name);
        }
      });
    });
  });
  
  if (!only_once) {
    setTimeout(fetch_images, IMAGES_UPDATE_INTERVAL);
  }
}
// Start Bot
DotaBot.init(function(err) {
  if (err) {
    return console.error('%s %s Failed to start DotaBot: %s', log_time(), '[ERROR]', err.message);
  }
  console.info('%s %s Started DotaBot successfully', log_time(), '[INFO]');
  
  fetch_schema();
  fetch_leagues();
  fetch_live_games();
  fetch_completed_games(true);
  fetch_heroes();
  fetch_items();
  fetch_images(true);
});


var ctrlServer = net.createServer(function(client) {
  client.on('data', function(chunk) {
    var command = chunk.toString().slice(0, -1); // Remove EOF character
    if (['fetch_leagues', 'fetch_live_games', 'fetch_completed_games', 'fetch_heroes',
      'fetch_items', 'fetch_images', 'fetch_schema', 'fetch_league_images'].indexOf(command) > -1)
      console.info('%s %s Received recognized command: %s', log_time(), '[INFO]', command);
    else
      console.info('%s %s Received unrecognized command: %s', log_time(), '[INFO]', command);
    switch (command) {
      case 'fetch_schema':
        fetch_schema(true);
        break;
      case 'fetch_leagues':
        fetch_leagues(true);
        break;
      case 'fetch_live_games':
        fetch_live_games(true);
        break;
      case 'fetch_completed_games':
        fetch_completed_games(true);
        break;
      case 'fetch_heroes':
        fetch_heroes(true);
        break;
      case 'fetch_items':
        fetch_items(true);
        break;
      case 'fetch_images':
        fetch_images(true, true);
        break;
      case 'fetch_league_images':
        fetch_league_images(true, true);
      default:
        break;
    }
  })
});
ctrlServer.listen(path.resolve(__dirname + '/.tmp/bot.sock'), function() {
  console.info('%s %s Start listening for command on .tmp/bot.sock', log_time(), '[INFO]');
});
ctrlServer.on('error', function (e) {
  if (e.code == 'EADDRINUSE') {
    console.warn('%s %s Socket in use, retrying...', log_time(), '[WARN]');
    fs.unlinkSync(path.resolve(__dirname + '/.tmp/bot.sock'));
    setTimeout(function () {
      ctrlServer.close();
      ctrlServer.listen(path.resolve(__dirname + '/.tmp/bot.sock'));
    }, 1000);
  }
});

// Handle interupt signal
var clean_and_close = function() {
  if (ctrlServer) {
    ctrlServer.close();
  }
  if (DotaBot.db != null) {
    console.info('%s %s Waiting for database connection to close', log_time(), '[INFO]');
    
    // Close database connection before exit
    DotaBot.db.close(function(err, result) {
      if (err) {
        console.error('%s %s Error while closing database connection: %s', log_time(), '[WARN]', err.message)
      } else {
        console.info('%s %s Closed database connection successfully', log_time(), '[INFO]')
      }
      // Now quit
      process.exit();
    });
  } else {
    // Now quit
    process.exit();
  }
}
process.on('SIGINT', clean_and_close);
process.on('SIGTERM', clean_and_close);

/* HELPER */
// Return current time in ISO string
var log_time = function() {
  return (new Date()).toISOString();
}
