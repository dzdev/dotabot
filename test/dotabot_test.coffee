###
dotabot_test.coffee
https://bitbucket.org/dzdev/dotabot

This file is part of dotabot
Copyright (c) 2015 DZ Team
Licensed under the LGPL-3.0 license.
###

'use strict'

should      = require 'should'
sinon       = require 'sinon'
Steam       = require 'steam-webapi'
fs          = require 'fs'
DBCleaner   = require 'database-cleaner'
bigJSON     = require '../lib/big_json_parse'
VDF         = require '../lib/vdf'
http        = require 'http'
request     = require 'request'
path        = require 'path'
execSync    = require('child_process').execSync
steam_data  = bigJSON.parse fs.readFileSync 'test/steam_data.json', 'utf8'
DotaBot     = null
db_cleaner  = null
    
describe 'DotaBot', ->
  beforeEach (done) ->
    # Prepare bot library
    DotaBot = require '../lib/dotabot'
    DotaBot.db_uri = 'mongodb://mongo:27017/dota2_test'
    
    # Stub Steam API wrapper
    sinon.stub Steam.prototype, "getSupportedAPIList", (steamObj, callback) ->
      callback null, steam_data
    done()
    
  afterEach (done) ->
    # Clean bot library
    delete require.cache[require.resolve('../lib/dotabot')]
    db_cleaner = new DBCleaner('mongodb')
    
    # Clean stub
    if Steam.prototype.getSupportedAPIList.restore
      Steam.prototype.getSupportedAPIList.restore()
    
    # Clean database
    if DotaBot.db
      db_cleaner.clean DotaBot.db, ->
        DotaBot = null
        done()
    else
      DotaBot = null;
      done();
    
  describe '::init()', ->
    it 'should prepare database connection', (done) ->
      DotaBot.init (err) ->
        should(DotaBot.db).be.ok
        done()
    
    it 'should prepare steam wrapper object', (done) ->
      DotaBot.init (err) ->
        should(DotaBot.steam).be.ok
        done()
  
  describe '::updateLeagues()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateLeagues (err) ->
        should(err).be.ok
        done()
        
    describe 'after init', ->
      beforeEach (done) ->
        DotaBot.init (err) ->
          # Stub Steam API
          sinon.stub DotaBot.steam, 'getLeagueListing', (steamObj, callback) ->
            callback(null, steam_data)
          done()
      
      afterEach (done) ->
        # Clean stub
        DotaBot.steam.getLeagueListing.restore()
        done()
      
      it 'should update leagues', (done) ->
        DotaBot.db.matches.insertOne steam_data.games[0], (err, result) ->
          last_match_timestamp = result.insertedId.getTimestamp()
          league_id = steam_data.games[0].league_id
          DotaBot.updateLeagues (err) ->
            DotaBot.db.collection('leagues').find().toArray (err, leagues) ->
              should(leagues.length).be.equal(steam_data.leagues.length); # Number of leagues in steam example data
              for league in leagues
                if league.league_id == league_id
                  should(league.last_match_at.getTime()).be.equal(last_match_timestamp.getTime())
              done();
  
  describe '::updateLiveLeagueGames()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateLiveLeagueGames (err) ->
        should(err).be.ok
        done()
        
    describe 'after init', ->
      beforeEach (done) ->
        DotaBot.init (err) ->
          # Stub Steam API
          sinon.stub DotaBot.steam, 'getLiveLeagueGames', (steamObj, callback) ->
            callback null, JSON.parse(JSON.stringify(steam_data)) # Clone example data to avoid modification from callback
          done()
      
      afterEach (done) ->
        DotaBot.steam.getLiveLeagueGames.restore()
        done()
        
      it 'should update matches', (done) ->
        DotaBot.updateLiveLeagueGames (err) ->
          DotaBot.db.collection('matches').find().count (err, count) ->
            should(count).be.equal(steam_data.games.length)
            done()
      
      it 'should update scoreboard', (done) ->
        DotaBot.updateLiveLeagueGames (err) ->
          DotaBot.db.collection('scoreboard').find().count (err, count) ->
            # Compute number of scoreboard from example data
            scoreboard_count = steam_data.games.reduce (previousValue, currentGame) ->
              if currentGame.scoreboard
                previousValue + 1
              else
                previousValue
            , 0

            should(count).be.equal scoreboard_count
            done()
      it 'should mark old matches as done', (done) ->
        match = { match_id: 12345 }
        DotaBot.db.matches.insertOne match, (err, result) ->
          DotaBot.updateLiveLeagueGames (err) ->
            DotaBot.db.matches.findOne { match_id: { $eq: match.match_id } }, (err, result) ->
              should(result.completed).be.equal(true)
              done()
    describe 'after init', ->
      beforeEach (done) ->
        DotaBot.init (err) ->
          # Stub Steam API
          sinon.stub DotaBot.steam, 'getLiveLeagueGames', (steamObj, callback) ->
            callback null, {"games": []}
          done()
      
      afterEach (done) ->
        DotaBot.steam.getLiveLeagueGames.restore()
        done()
      
      it 'should successfully call callback', (done) ->
        DotaBot.updateLiveLeagueGames (err) ->
          done()
  
  describe '::updateCompletedMatches()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateCompletedMatches (err) ->
        should(err).be.ok
        done()
    
    it 'should update completed matches', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getMatchDetails', (steamObj, callback) ->
          callback null, steam_data.match
        
        match = { match_id: steam_data.match.match_id, completed: true }
        DotaBot.db.matches.insertOne match, (err, result) ->
          DotaBot.updateCompletedMatches (err) ->
            DotaBot.db.matches.findOne { match_id: steam_data.match.match_id }, (err, result) ->
              should(result.radiant_win).be.equal(steam_data.match.radiant_win)
              done()
  
  describe '::updateTeam()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateTeam steam_data.teams[0].team_id, (err) ->
        should(err).be.ok
        done()
  
    it 'should throw error if not provided team_id', (done) ->
      DotaBot.init (err) ->
        DotaBot.updateTeam null, (err) ->
          should(err).be.ok
          done()
    
    it 'should update team', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getTeamInfoByTeamID', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))

        team_id = steam_data.teams[0].team_id
        DotaBot.updateTeam team_id, (err) ->
          DotaBot.db.collection('teams').find({team_id: {$eq: team_id}}).count (err, count) ->
            should(count).be.equal 1
            
            DotaBot.steam.getTeamInfoByTeamID.restore()
            done()
    
    # TODO: Re-test this
    it 'should force update team', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getTeamInfoByTeamID', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))

        team_id = steam_data.teams[0].team_id
        DotaBot.updateTeam team_id, true, (err) ->
          DotaBot.db.collection('teams').find({team_id: {$eq: team_id}}).count (err, count) ->
            should(count).be.equal 1
            
            DotaBot.steam.getTeamInfoByTeamID.restore()
            done()
  
  describe '::updateTeams()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateTeams (err) ->
        should(err).be.ok
        done()
    
    it 'should update teams from matches collection', (done) ->
      DotaBot.init (err) ->
        # Seed matches into database
        matches = steam_data.games.map (game) ->
          game_clone = JSON.parse(JSON.stringify(game))
          if game_clone.scoreboard
            delete game_clone.scoreboard
          game_clone
        team_count = matches.reduce (previousValue, match) ->
          if match.radiant_team && match.radiant_team.team_id
            previousValue += 1
          if match.dire_team && match.dire_team.team_id
            previousValue += 1
          previousValue
        , 0
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getTeamInfoByTeamID', (steamObj, callback) ->
          # Randomize team_id
          steam_data_clone = JSON.parse(JSON.stringify(steam_data))
          steam_data_clone.teams[0].team_id = Math.floor((Math.random() * 1000000))
          callback null, steam_data_clone
        DotaBot.db.collection('matches').insertMany matches, (err, result) ->
          DotaBot.updateTeams (err) ->
            DotaBot.db.collection('teams').find().count (err, count) ->
              should(count).be.equal team_count
              
              DotaBot.steam.getTeamInfoByTeamID.restore()
              done()
              
  describe '::updatePlayers', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updatePlayers (err) ->
        should(err).be.ok
        done()
    
    it 'should update players from matches collection', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getPlayerSummaries', (steamObj, callback) ->
          ids = steamObj.steamids.split ','
          players = ids.map (id) ->
            player_clone = JSON.parse(JSON.stringify(steam_data.players[0]))
            player_clone.steamid = id
            player_clone
            
          callback null,
            players: players
          
        # Seed matches into database
        matches = steam_data.games.map (game) ->
          game_clone = JSON.parse(JSON.stringify(game))
          if game_clone.scoreboard
            delete game_clone.scoreboard
          game_clone
        player_count = matches.reduce (previousValue, match) ->
          previousValue += match.players.length
        , 0
        DotaBot.db.collection('matches').insertMany matches, (err, result) ->
          DotaBot.updatePlayers (err) ->
            DotaBot.db.collection('players').find().count (err, count) ->
              should(count).be.equal player_count
              
              done()
  
  describe '::updateSchema()', ->
    beforeEach (done) ->
      res = fs.readFileSync __dirname + '/items_game.txt', 'utf8'
      sinon.stub request, 'get'
        .yields null, null, res
      done()
    
    afterEach (done) ->
      request.get.restore()
      done()
      
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateSchema (err) ->
        should(err).be.ok
        done()
    
    it 'should update schema', (done) ->
      DotaBot.init (err) ->
        sinon.stub DotaBot.steam, 'getSchemaURL', (steamObj, callback) ->
          callback null, steam_data.schema_url
        
        DotaBot.updateSchema (err) ->
          DotaBot.db.schema.findOne (err, data) ->
            should(data).be.ok
            DotaBot.steam.getSchemaURL.restore()
            done()
            
  describe '::downloadFileByID()', ->
    file_id = steam_data.teams[0].logo
    file_path = __dirname + '/teams/logo.png'
    
    beforeEach (done) ->
      sinon.stub request, 'get', (url) ->
        fs.createReadStream __dirname + '/logo.png'
      done()
        
    afterEach (done) ->
      if request.get.restore
        request.get.restore()
      done()
    
    it 'should throw error if not init yet', (done) ->
      DotaBot.downloadFileByID { file_id: file_id, file_path: file_path }, (err) ->
        should(err).be.ok
        done()
    
    it 'should require file_id', (done) ->
      DotaBot.init (err) ->
        DotaBot.downloadFileByID { file_id: null, file_path: file_path }, (err) ->
          should(err).be.ok
          done()
    
    it 'should require file_path', (done) ->
      DotaBot.init (err) ->
        DotaBot.downloadFileByID { file_id: file_id, file_path: null }, (err) ->
          should(err).be.ok
          done()
    
    it 'should download file', (done) ->
      DotaBot.init (err) ->
        # Stub steam API
        sinon.stub DotaBot.steam, 'getUGCFileDetails', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
          
        DotaBot.downloadFileByID { file_id: file_id, file_path: file_path }, (err) ->
          fs.access file_path, fs.F_OK, (err) ->
            should(err).not.be.ok
            
            # Clean stub
            DotaBot.steam.getUGCFileDetails.restore()
            
            # Clean created file and folder
            fs.unlink file_path
            fs.rmdir path.dirname(file_path)
            done()
    
    it 'should not overwrite if file exists', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        fs.closeSync fs.openSync(file_path, 'w')
      catch e
        # Ignore error
      
      DotaBot.init (err) ->
        # Stub steam API
        sinon.stub DotaBot.steam, 'getUGCFileDetails', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
          
        DotaBot.downloadFileByID { file_id: file_id, file_path: file_path }, (err) ->
          fs.stat file_path, (err, stats) ->
            should(err).not.be.ok
            
            should(stats.size).be.equal 0
            
            # Clean stub
            DotaBot.steam.getUGCFileDetails.restore()
            
            # Clean created file and folder
            fs.unlink file_path
            fs.rmdir path.dirname(file_path)
            done()
      
    it 'shoud overwrite if specified even when file exists', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        fs.closeSync fs.openSync(file_path, 'w')
      catch e
        # Ignore error
      
      DotaBot.init (err) ->
        # Stub steam API
        sinon.stub DotaBot.steam, 'getUGCFileDetails', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
          
        DotaBot.downloadFileByID { file_id: file_id, file_path: file_path, overwrite: true }, (err) ->
          fs.stat file_path, (err, stats) ->
            should(err).not.be.ok
            
            should(stats.size).be.above 0
            
            # Clean stub
            DotaBot.steam.getUGCFileDetails.restore()
            
            # Clean created file and folder
            fs.unlink file_path
            fs.rmdir path.dirname(file_path)
            done()
            
    it 'should overwrite if file is corrupted when using imagemagick', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        execSync "cp #{__dirname + '/logo_corrupted.png'} #{file_path}"
      catch e
        # Ignore error
        
      DotaBot.init (err) ->
        # Stub steam API
        sinon.stub DotaBot.steam, 'getUGCFileDetails', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
          
        DotaBot.downloadFileByID { file_id: file_id, file_path: file_path, im_check: true }, (err) ->
          fs.stat file_path, (err, stats) ->
            should(err).not.be.ok
            
            fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
              should(stats.size).not.be.equal orig_stats.size
              
              # Clean stub
              DotaBot.steam.getUGCFileDetails.restore()
              
              # Clean created file and folder
              fs.unlink file_path
              fs.rmdir path.dirname(file_path)
              done()
    
    it 'should retry if downloaded file is corrupted', (done) ->
      request.get.restore()
      called = 0
      sinon.stub request, 'get', (url) ->
        # Read from file instead of download from Steam
        if called == 0
          logo = fs.createReadStream __dirname + '/logo_corrupted.png' # Corrupted file
          called += 1
        else
          logo = fs.createReadStream __dirname + '/logo.png' # Not corrupted file
        
        logo
        
      DotaBot.init (err) ->
        # Stub steam API
        sinon.stub DotaBot.steam, 'getUGCFileDetails', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
          
        DotaBot.downloadFileByID { file_id: file_id, file_path: file_path }, (err) ->
          fs.stat file_path, (err, stats) ->
            should(err).not.be.ok
            
            fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
              should(stats.size).not.be.equal orig_stats.size
              
              # Clean created file and folder
              fs.unlink file_path, ->
                fs.rmdir path.dirname(file_path), ->
                  done()
            
  describe '::downloadFileByURL()', ->
    file_url = 'http://example.com/file.png'
    file_path = __dirname + '/teams/logo.png'
    
    beforeEach (done) ->
      sinon.stub request, 'get', (url) ->
        fs.createReadStream __dirname + '/logo.png'
      done()
        
    afterEach (done) ->
      if request.get.restore
        request.get.restore()
      done()
    
    it 'should require file_url', (done) ->
      DotaBot.init (err) ->
        DotaBot.downloadFileByURL { file_url: null, file_path: file_path }, (err) ->
          should(err).be.ok
          done()
    
    it 'should require file_path', (done) ->
      DotaBot.init (err) ->
        DotaBot.downloadFileByURL { file_url: file_url, file_path: null }, (err) ->
          should(err).be.ok
          done()
    
    it 'should download file', (done) ->
      DotaBot.downloadFileByURL { file_url: file_url, file_path: file_path }, (err) ->
        fs.access file_path, fs.F_OK, (err) ->
          should(err).not.be.ok
          
          # Clean created file and folder
          fs.unlink file_path
          fs.rmdir path.dirname(file_path)
          done()
    
    it 'should not overwrite if file exists', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        fs.closeSync fs.openSync(file_path, 'w')
      catch e
        # Ignore error

      DotaBot.downloadFileByURL { file_url: file_url, file_path: file_path }, (err) ->
        fs.stat file_path, (err, stats) ->
          should(err).not.be.ok
          
          should(stats.size).be.equal 0
          
          # Clean created file and folder
          fs.unlink file_path
          fs.rmdir path.dirname(file_path)
          done()
    
    it 'should overwrite if specified', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        fs.closeSync fs.openSync(file_path, 'w')
      catch e
        # Ignore error
          
      DotaBot.downloadFileByURL { file_url: file_url, file_path: file_path, overwrite: true }, (err) ->
        fs.stat file_path, (err, stats) ->
          should(err).not.be.ok
          
          should(stats.size).not.be.equal 0
          
          # Clean created file and folder
          fs.unlink file_path, ->
            fs.rmdir path.dirname(file_path), ->
              done()
          
    it 'should overwrite if file is corrupted using imagemagick to check', (done) ->
      try
        fs.mkdirSync __dirname + '/teams'
        execSync "cp #{__dirname + '/logo_corrupted.png'} #{file_path}"
      catch e
        # Ignore error
      
      DotaBot.downloadFileByURL { file_url: file_url, file_path: file_path, im_check: true }, (err) ->
        fs.stat file_path, (err, stats) ->
          should(err).not.be.ok
          
          fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
            should(stats.size).not.be.equal orig_stats.size
            
            # Clean created file and folder
            fs.unlink file_path, ->
              fs.rmdir path.dirname(file_path), ->
                done()
            
    it 'should retry if downloaded file is corrupted', (done) ->
      request.get.restore()
      called = 0
      sinon.stub request, 'get', (url) ->
        # Read from file instead of download from Steam
        if called == 0
          logo = fs.createReadStream __dirname + '/logo_corrupted.png' # Corrupted file
          called += 1
        else
          logo = fs.createReadStream __dirname + '/logo.png' # Not corrupted file
        
        logo
        
      DotaBot.downloadFileByURL { file_url: file_url, file_path: file_path }, (err) ->
        fs.stat file_path, (err, stats) ->
          should(err).not.be.ok
          
          fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
            should(stats.size).not.be.equal orig_stats.size
            
            # Clean created file and folder
            fs.unlink file_path, ->
              fs.rmdir path.dirname(file_path), ->
                done()
  
  describe '::downloadFileByItemDef()', ->
    item_id = 10541
    item_name = 'subscriptions_fun'
    file_path = __dirname + '/leagues/logo.png'
    
    it 'should throw error if not init yet', (done) ->
      DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path }, (err) ->
        should(err).be.ok
        done()
    
    describe 'after init', ->
      beforeEach (done) ->
        DotaBot.init (err) ->
          done()
      
      afterEach (done) ->
        DotaBot.db = null
        DotaBot.steam = null
        done()
      
      it 'should require item_id', (done) ->
        DotaBot.downloadFileByItemDef { item_id: null, file_path: file_path }, (err) ->
          should(err).be.ok
          done()
      
      it 'should require file_path', (done) ->
        DotaBot.downloadFileByItemDef { item_id: item_id, file_path: null }, (err) ->
          should(err).be.ok
          done()
          
      it 'should check for schema db', (done) ->
        DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path }, (err) ->
          should(err).be.ok
          done()
      
      describe 'and schema ready', ->
        beforeEach (done) ->
          sinon.stub DotaBot.db.schema, 'findOne', (callback) ->
            data = VDF.parse fs.readFileSync(__dirname + '/items_game.txt', 'UTF-8')
            callback null, data.items_game
          sinon.stub DotaBot.steam, 'getItemIconPath', (steamObj, callback) ->
            callback null, steam_data.icon_path
          sinon.stub request, 'get', (url) ->
            fs.createReadStream __dirname + '/logo.png'
          done()
        afterEach (done) ->
          DotaBot.db.schema.findOne.restore()
          DotaBot.steam.getItemIconPath.restore()
          request.get.restore()
          
          done()
        
        it 'should download file', (done) ->
          DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path }, (err) ->
            fs.stat file_path, (err, stats) ->
              should(err).not.be.ok
              
              # Cleanup
              fs.unlink file_path, ->
                fs.rmdir __dirname + '/leagues', ->
                  done()
        
        it 'should not overwrite existing file', (done) ->
          try
            fs.mkdirSync path.dirname(file_path)
            fs.closeSync fs.openSync(file_path, 'w')
          catch e
            # Ignore error
              
          DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path }, (err) ->
            fs.stat file_path, (err, stats) ->
              should(err).not.be.ok
              
              should(stats.size).be.equal 0
              
              # Clean created file and folder
              fs.unlink file_path, ->
                fs.rmdir path.dirname(file_path), ->
                  done()
        
        it 'should overwrite if specified', (done) ->
          try
            fs.mkdirSync path.dirname(file_path)
            fs.closeSync fs.openSync(file_path, 'w')
          catch e
            # Ignore error
              
          DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path, overwrite: true }, (err) ->
            fs.stat file_path, (err, stats) ->
              should(err).not.be.ok
              
              should(stats.size).not.be.equal 0
              
              # Clean created file and folder
              fs.unlink file_path, ->
                fs.rmdir path.dirname(file_path), ->
                  done()
        
        it 'should retry if downloaded file is corrupted', (done) ->
          request.get.restore()
          called = 0
          sinon.stub request, 'get', (url) ->
            # Read from file instead of download from Steam
            if called == 0
              logo = fs.createReadStream __dirname + '/logo_corrupted.png' # Corrupted file
              called += 1
            else
              logo = fs.createReadStream __dirname + '/logo.png' # Not corrupted file
            
            logo
            
          DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path }, (err) ->
            fs.stat file_path, (err, stats) ->
              should(err).not.be.ok
              
              fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
                should(stats.size).not.be.equal orig_stats.size
                
                # Clean created file and folder
                fs.unlink file_path, ->
                  fs.rmdir path.dirname(file_path), ->
                    done()
        
        it 'should overwrite if file is corrupted using imagemagick to check', (done) ->
          try
            fs.mkdirSync __dirname + '/leagues'
            execSync "cp #{__dirname + '/logo_corrupted.png'} #{file_path}"
          catch e
            # Ignore error
          
          DotaBot.downloadFileByItemDef { item_id: item_id, file_path: file_path, im_check: true }, (err) ->
            fs.stat file_path, (err, stats) ->
              should(err).not.be.ok
              
              fs.stat __dirname + '/logo_corrupted.png', (err, orig_stats) ->
                should(stats.size).not.be.equal orig_stats.size
                
                # Clean created file and folder
                fs.unlink file_path, ->
                  fs.rmdir path.dirname(file_path), ->
                    done()
            
  describe '::updateHeroes()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateHeroes (err) ->
        should(err).be.ok
        done()
    
    it 'should update heroes', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getHeroes', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
        
        DotaBot.updateHeroes (err) ->
          DotaBot.db.collection('heroes').find().count (err, count) ->
            should(count).be.equal steam_data.heroes.length
            
            DotaBot.steam.getHeroes.restore()
            done()
      
  describe '::updateItems()', ->
    it 'should throw error if not init yet', (done) ->
      DotaBot.updateItems (err) ->
        should(err).be.ok
        done()
    
    it 'should update items', (done) ->
      DotaBot.init (err) ->
        # Stub Steam API
        sinon.stub DotaBot.steam, 'getGameItems', (steamObj, callback) ->
          callback null, JSON.parse(JSON.stringify(steam_data))
        
        DotaBot.updateItems (err) ->
          DotaBot.db.collection('items').find().count (err, count) ->
            should(count).be.equal steam_data.items.length
            
            DotaBot.steam.getGameItems.restore()
            done()
            
  describe '::convertAccountIDtoSteamID()', ->
    it 'should convert 32-bit ID to 64-bit ID', ->
      account_id    = '166715230'
      steam_id      = '76561198126980958'
      converted_id  = DotaBot.convertAccountIDtoSteamID(account_id)
      should(converted_id).be.equal(steam_id)
