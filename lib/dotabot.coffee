###
dotabot.coffee
https://bitbucket.org/dzdev/dotabot

This file is part of dotabot
Copyright (c) 2015 DZ Team
Licensed under the LGPL-3.0 license.
###

'use strict'

MongoClient = require('mongodb').MongoClient
ObjectID    = require('mongodb').ObjectID
Steam = require 'steam-webapi'
VDF   = require __dirname + '/vdf'
http  = require 'http'
https = require 'https'
request = require 'request'
fs    = require 'fs'
mkdirp = require 'mkdirp'
path   = require 'path'
bignum = require 'bignum'
exec   = require('child_process').exec
Bot =
  'db_uri': null
  'db':     null
  'steam':  null
  'key':    null
  'updated_teams': []

Object.defineProperties exports,
  'db_uri':
    get: ->
      Bot.db_uri
    set: (uri) ->
      Bot.db_uri = uri
  'db':
    get: ->
      Bot.db
    set: (obj) ->
      Bot.db = obj
  'steam':
    get: ->
      Bot.steam
    set: (obj) ->
      Bot.steam = obj
  'key':
    get: ->
      Bot.key
    set: (obj) ->
      Bot.key = obj

exports.init = (callback) ->
  MongoClient.connect Bot.db_uri, (err, database) ->
    if err
      return callback(err)
    Bot.db = database
    exports.db = Bot.db
    
    # Prepare leagues table
    Bot.db.leagues = Bot.db.collection('leagues')
    # Add index to league_id field
    Bot.db.leagues.createIndex { league_id: 1 }, { unique: true }, (err, result) ->
      if err
        throw err
    
    # Prepare matches table
    Bot.db.matches = Bot.db.collection('matches')
    # Add index to match_id field
    Bot.db.matches.createIndex { match_id: 1 }, { unique: true }, (err, result) ->
      if err
        throw err
    
    # Prepare scoreboard table
    Bot.db.scoreboard = Bot.db.collection('scoreboard')
    # Add index to match_id and duration field
    Bot.db.scoreboard.createIndex { match_id: 1, duration: 1 }, { unique: true }, (err, result) ->
      if err
        throw err
    
    # Prepare teams table
    Bot.db.teams = Bot.db.collection('teams')
    # Add index to team_id
    Bot.db.teams.createIndex { team_id: 1 }, { unique: true }, (err, result) ->
      if err
        throw err
    
    # Prepare players table
    Bot.db.players = Bot.db.collection('players')
    # Add index to team_id
    Bot.db.players.createIndex { account_id: 1 }, { unique: true }, (err, result) ->
      if err
        throw err
    
    # Prepare heroes table
    Bot.db.heroes = Bot.db.collection('heroes')
    # Add index to hero_id
    Bot.db.heroes.createIndex { hero_id: 1 }, { unique: true}, (err, result) ->
      if err
        throw err
        
    # Prepare items table
    Bot.db.items = Bot.db.collection('items')
    # Add index to hero_id
    Bot.db.items.createIndex { item_id: 1 }, { unique: true}, (err, result) ->
      if err
        throw err
    
    # Prepare schema table
    Bot.db.schema = Bot.db.collection('schema')
    
    # Prepare cache
    Bot.cache = {
      live: {}
    }
    
    # Prepare steam wrappers
    Steam.key = Bot.key || process.env.STEAM_API_KEY
    Steam.language = 'english'
    Steam.gameid = Steam.DOTA2
    Steam.appid  = Steam.DOTA2
    Steam.ready (err) ->
      if err
        return callback(err)
      Bot.steam = new Steam()
      callback()

exports.updateLeagues = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  Bot.steam.getLeagueListing {}, (err, data) ->
    if err
      return callback err
      
    bulk = Bot.db.leagues.initializeUnorderedBulkOp()
    
    count = 0
    data.leagues.forEach (league) ->
      # Change leagueid property name for consistency
      league.league_id = league.leagueid
      delete league.leagueid
      # Search for last match of this league
      Bot.db.matches.findOne { league_id: league.league_id },
        { sort: { '_id': -1 }, fields: { '_id': 1 }}, (err, last_match) ->
          if last_match
            league.last_match_at = (new ObjectID(last_match._id)).getTimestamp()
          else
            league.last_match_at = null
          
          # Queueing update operation
          bulk.find { league_id: { $eq: league.league_id } }
            .upsert()
            .updateOne league
          
          # If this is the last one, then execute update operation
          count += 1
          if count == data.leagues.length
            bulk.execute (err, result) ->
              if err
                callback err
              else
                callback null, result

exports.updateLiveLeagueGames = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  Bot.steam.getLiveLeagueGames {}, (err, data) ->
    if err
      return callback err
      
    # If there is no live games, just quit
    if data.games.length < 1
      # Mark old matches as done
      Bot.db.matches.updateMany { completed: { $eq: false } },
        { $set: { completed: true } }, (err, result) ->
          # Ignore error
          return callback()
    else
      # Prepare matches queue and scores queue
      match_bulk = Bot.db.matches.initializeUnorderedBulkOp()
      scoreboard_bulk = null
      data.games.forEach (game) ->
        # Check for scoreboard
        if game.scoreboard != undefined
          score = game.scoreboard
          score.match_id = game.match_id
          score.duration = Math.floor score.duration # MongoDB can't compare float, and we doesn't need the duration smaller than second
          game.current_score = {
            radiant: game.scoreboard.radiant.score,
            dire: game.scoreboard.dire.score
          }
          game.duration = game.scoreboard.duration
          
          # If the same duration is recorded 3 times,
          # we consider this game as paused
          if !Bot.cache.live[game.match_id]
            Bot.cache.live[game.match_id] =
              last_duration: score.duration
              saw_same_duration: 0
            game_cache = Bot.cache.live[game.match_id]
          else
            game_cache = Bot.cache.live[game.match_id]
            if game_cache.last_duration == game.duration
              game_cache.saw_same_duration += 1
            else
              game_cache.last_duration = game.duration
              game_cache.saw_same_duration = 0
          if game_cache.saw_same_duration > 2 && game_cache.last_duration > 0
            game.paused = true
          else
            game.paused = false
            
          game.roshan_respawn_timer = game.scoreboard.roshan_respawn_timer
          game.completed = false
          # Detect pause game
          
          delete game.scoreboard
          
          # Queue score
          if !scoreboard_bulk
            scoreboard_bulk = Bot.db.scoreboard.initializeUnorderedBulkOp()
          scoreboard_bulk.find { match_id: { $eq: score.match_id }, duration: { $eq: score.duration } }
            .upsert()
            .updateOne score
        
        # Queue match
        match_bulk.find { match_id: { $eq: game.match_id } }
          .upsert()
          .updateOne game
      
      # Save matches and scores to database
      match_bulk.execute (err, result) ->
        match_result = result
        if err
            return callback(new Error(err.errmsg))
        if scoreboard_bulk
          scoreboard_bulk.execute (err, result) ->
            scoreboard_result = result
            if err
              return callback(new Error(err.errmsg))
            # Mark old matches as done
            Bot.db.matches.updateMany { match_id: { $nin: data.games.map((game) -> game.match_id ) } },
              { $set: { completed: true } }, (err, result) ->
                # Ignore error
                callback(null, match_result, scoreboard_result)
        else
          # Mark old matches as done
          Bot.db.matches.updateMany { match_id: { $nin: data.games.map((game) -> game.match_id ) } },
            { $set: { completed: true } }, (err, result) ->
              # Ignore error
              callback(null, match_result)
    
exports.updateCompletedMatches = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  # Search for completed matches without radiant_win attribute
  Bot.db.matches.find({ completed: true, radiant_win: { $exists: false } }).toArray (err, result) ->
    count = result.length
    updated = 0
    errors = []
    if count > 0
      result.forEach (match) ->
        Bot.steam.getMatchDetails { match_id: match.match_id }, (err, result) ->
          if err
            errors.push match.match_id
            updated = updated + 1
            if count == updated
              if errors.length > 0
                return callback(new Error('Failed to update ' + errors.join()), { nModified: count - errors.length })
              else
                return callback null, { nModified: count }
          else
            Bot.db.matches.updateOne { match_id: match.match_id }, { $set: { radiant_win: result.radiant_win } }, (err, result) ->
              if err
                errors.push match.match_id
              updated = updated + 1
              if count == updated
                if errors.length > 0
                  return callback(new Error('Failed to update ' + errors.join()), { nModified: count - errors.length })
                else
                  return callback null, { nModified: count }
        
        # Clean cache
        if Bot.cache.live[match.match_id]
          Bot.cache.live[match.match_id] = null
    else
      callback null, { nModified: count }

exports.updateTeam = (team_id, force_update, callback) ->
  if typeof force_update == 'function'
    callback = force_update
    force_update = false
  
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))

  if !team_id
    return callback(new Error('Please provide team ID'))
  
  if !force_update
    # Find the team in retrieved teams buffer
    if Bot.updated_teams.indexOf(team_id) != -1
      return callback()
  
    # Find the team in database, if exists then add it to the buffer so we
    # won't need to access db again
    Bot.db.teams.find({ team_id: { $eq: team_id } }).count (err, count) ->
      if err
        return callback(err)
      
      if count > 0
        Bot.updated_teams.push team_id
        return callback()
      
      # Now that this team does not exists yet, we get it from Steam
      Bot.steam.getTeamInfoByTeamID
        teams_requested: 1
        start_at_team_id: team_id
      , (err, data) ->
        if err
          return callback(err)
        
        # Store team to database
        Bot.db.teams.insert data.teams[0], (err, result) ->
          if err
              return callback(new Error(err))
          Bot.updated_teams.push team_id
          callback(null, result)
  else
    Bot.steam.getTeamInfoByTeamID
      teams_requested: 1
      start_at_team_id: team_id
    , (err, data) ->
      if err
        return callback(err)
      Bot.db.teams.findOneAndUpdate { team_id: { $eq: team_id } },
        data.teams[0], {upsert: true}, (err, result) ->
          if err
            return callback(err)
          callback(null, result)

exports.updateTeams = (callback) ->
  if !Bot.db
    return callback(new Error('Please call DotaBot.init() first'))
  
  team_ids = []
  
  # Find teams from matches collection
  Bot.db.matches.distinct "radiant_team.team_id", (err, radiant_ids) ->
    if err
      return callback(err)
    
    team_ids = team_ids.concat radiant_ids
    
    Bot.db.matches.distinct "dire_team.team_id", (err, dire_ids) ->
      if err
        return callback(err)
      team_ids = team_ids.concat dire_ids
      
      if team_ids.length > 0
        completed_count = 0
        new_count = 0
        team_ids.forEach (team_id) ->
            exports.updateTeam team_id, (err, team_result) ->
              # Ignore errors
              completed_count += 1
              
              if team_result && team_result.result.ok == 1
                new_count += 1
              if completed_count == team_ids.length
                callback null, { nUpserted: new_count }
      else
        callback null, { nUpserted: 0 }

exports.updatePlayers = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))

  # Find players from matches collection
  Bot.db.matches.distinct "players.account_id", (err, new_player_ids) ->
    if err
      return callback err
    
    # Find existing players
    Bot.db.players.distinct "account_id", (err, old_player_ids) ->
      if err
        return callback err
      
      # Don't update existing players
      for id in old_player_ids
        i = new_player_ids.indexOf id
        if i != -1
          new_player_ids.splice i, 1
      
      # TODO Handle unique ids
      steam_ids = new_player_ids.map (player_id) -> exports.convertAccountIDtoSteamID player_id
      if steam_ids.length > 0
        Bot.steam.getPlayerSummaries steamids: steam_ids.join(','), (err, data) ->
          # Match account_id with steamid
          data.players.forEach (player) ->
            player.account_id = new_player_ids[steam_ids.indexOf(player.steamid)]
          Bot.db.players.insertMany data.players, (err, result) ->
            if err
              callback err
            callback null, result
      else
        callback null, { result: { ok: 1, n:0 } }

exports.updateSchema = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  Bot.steam.getSchemaURL {}, (err, data) ->
    if err
      return callback err
    
    schema_vdf = null
    request.get data.items_game_url, (error, response, body) ->
      if error
        return callback error
      schema = VDF.parse body
      # Clean out items which have dotted key field name
      for key, item of schema.items_game.items
        if item.portraits
          delete item.portraits
        if item.prefab == 'bundle'
          delete schema.items_game.items[key]
      delete schema.items_game.loot_lists
      
      Bot.db.schema.findOneAndReplace {}, schema.items_game, {upsert: true}, (err, result) ->
        if err
          return callback err
        callback null, result
  
_im_processing_count = 0
_im_processing_limit = 30
_im_backlog = []
_im_check = (file_path, callback) ->
  # We only spawn around 30 processes at once to avoid ulimit
  # Other request to imagemagick will be pushed to a backlog
  if _im_processing_count > _im_processing_limit
    if file_path && callback
      _im_backlog.push [file_path, callback]
  else
    if file_path && callback
      _im_processing_count += 1
      exec "identify -verbose #{file_path}", (error) ->
        _im_processing_count -= 1
        if _im_processing_count < _im_processing_limit
          setTimeout _im_check, 0
        callback error
    else
      if _im_backlog.length > 0
        item = _im_backlog.shift()
        setTimeout (-> _im_check item[0], item[1]), 0

_dl_count = 0
_dl_limit = 10
_dl_backlog = []
_download = (file_url, file_path, callback) ->
  # We only download around 10 images at once to avoid
  # corrupted images because of network throttle
  if _dl_count > _dl_limit
    if file_url && file_path && callback
      _dl_backlog.push [file_url, file_path, callback]
  else
    if file_url && file_path && callback
      _dl_count += 1
      # Prepare directory
      mkdirp path.dirname(file_path), (err) ->
        if err
          return callback(err)
        
        # Prepare write stream to save file
        stream = fs.createWriteStream file_path
        
        request.get file_url
          .pipe stream
          .on 'error', (err) ->
            _dl_count -= 1
            if _dl_count < _dl_limit
              setTimeout _download, 0
            fs.unlink file_path, -> # Ignore callback
            callback err
          .on 'finish', () ->
            _dl_count -= 1
            if _dl_count < _dl_limit
              setTimeout _download, 0
            callback null, true
    else
      if _dl_backlog.length > 0
        item = _dl_backlog.shift()
        setTimeout (-> _download item[0], item[1], item[2]), 0
          
exports.downloadFileByID = (options, callback) ->
  if !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  # Handle default options
  opts = {
    file_id:   if options && options.file_id   then options.file_id   else null
    file_path: if options && options.file_path then options.file_path else null
    overwrite: if options && options.overwrite then options.overwrite else false
    im_check:  if options && options.im_check  then options.im_check  else false
  }
  if !opts.file_id
    return callback(new Error('Please specify file ID'))
  
  if !opts.file_path
    return callback(new Error('Please specify file save path'))
    
  if !opts.im_check
    # Check for file existence
    fs.access opts.file_path, fs.F_OK, (err) ->
      # Only overwrite file if specified
      if opts.overwrite || (!opts.overwrite && err)
        # Get file details from Steam
        Bot.steam.getUGCFileDetails { ugcid: opts.file_id }, (err, result) ->
          if err
            return callback(err)
          
          # Proxying
          _download result.data.url, opts.file_path, ->
            _im_check opts.file_path, (error) ->
              if error
                _download result.data.url, opts.file_path, callback
              else
                callback null, true
      else
        callback null, false
  else
    # Check for file existence and corruption using ImageMagick
    _im_check opts.file_path, (error) ->
      if error || (!error && opts.overwrite)
        # Get file details from Steam
        Bot.steam.getUGCFileDetails { ugcid: opts.file_id }, (err, result) ->
          if err
            return callback(err)
  
          # Proxying
          _download result.data.url, opts.file_path, callback
      else
        callback null, false
        
exports.downloadFileByURL = (options, callback) ->
  # Handle default options
  opts = {
    file_url:  if options && options.file_url  then options.file_url  else null
    file_path: if options && options.file_path then options.file_path else null
    overwrite: if options && options.overwrite then options.overwrite else false
    im_check:  if options && options.im_check  then options.im_check  else false
  }
  if !opts.file_url
    return callback(new Error('Please specify file URL'))
  
  if !opts.file_path
    return callback(new Error('Please specify file save path'))
  
  if !opts.im_check
    # Check for file existence
    fs.access opts.file_path, fs.F_OK, (err) ->
      # Only overwrite file if specified
      if opts.overwrite || (!opts.overwrite && err)
        _download opts.file_url, opts.file_path, ->
          _im_check opts.file_path, (error) ->
            if error
              _download opts.file_url, opts.file_path, callback
            else
              callback null, true
      else
        callback null, false
  else
    # Check for file existence and corruption using ImageMagick
    _im_check opts.file_path, (error) ->
      if error || (!error && opts.overwrite)
        _download opts.file_url, opts.file_path, callback
      else
        callback null, false
        
exports.downloadFileByItemDef = (options, callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  # Handle default options
  opts = {
    item_id:   if options && options.item_id   then options.item_id   else null
    file_path: if options && options.file_path then options.file_path else null
    overwrite: if options && options.overwrite then options.overwrite else false
    im_check:  if options && options.im_check  then options.im_check  else false
  }
  if !opts.item_id
    return callback(new Error('Please specify file URL'))
  
  if !opts.file_path
    return callback(new Error('Please specify file save path'))
  
  get_schema_and_download = ->
    Bot.db.schema.findOne (err, schema) ->
      if err
        return callback err
      if !schema
        return callback new Error('Schema is not ready')
      
      if schema.items[opts.item_id]
        paths = schema.items[opts.item_id].image_inventory.split('/')
        iconname = paths[paths.length - 1]
        Bot.steam.getItemIconPath { iconname: iconname }, (err, data) ->
          if (err)
            return callback err
          _download "http://cdn.dota2.com/apps/570/#{data.path}", opts.file_path, (err) ->
            _im_check opts.file_path, (error) ->
              if error
                _download "http://cdn.dota2.com/apps/570/#{data.path}", opts.file_path, callback
              else
                callback null, true
      else
        callback new Error("Item #{opts.item_id} is not yet available in schema")
        
  if !opts.im_check
    # Check for file existence
    fs.access opts.file_path, fs.F_OK, (err) ->
      # Only overwrite file if specified
      if opts.overwrite || (!opts.overwrite && err)
        get_schema_and_download()
      else
        callback null, false
  else
    # Check for file existence and corruption using ImageMagick
    _im_check opts.file_path, (error) ->
      if error || (!error && opts.overwrite)
        get_schema_and_download()
      else
        callback null, false

exports.updateHeroes = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  # Request heroes data from Steam API
  Bot.steam.getHeroes {}, (err, data) ->
    if err
      return callback(err)

    # Init bulk operation
    bulk = Bot.db.heroes.initializeUnorderedBulkOp()
    
    for hero in data.heroes
      do (hero) ->
        # Re-mapping name of some properties
        renameProperty hero, 'id', 'hero_id'
        renameProperty hero, 'localized_name', 'dname'
        hero.name = hero.name.replace 'npc_dota_hero_', ''
        
        # Queue data into update operation
        bulk.find { hero_id: { $eq: hero.hero_id } }
          .upsert()
          .updateOne hero
    
    # Execute database operation
    bulk.execute (err, result) ->
      if err
        callback(new Error(err.errmsg))
      callback null, result

exports.updateItems = (callback) ->
  if !Bot.db || !Bot.steam
    return callback(new Error('Please call DotaBot.init() first'))
  
  # Request items data from Steam API
  Bot.steam.getGameItems {}, (err, data) ->
    if err
      return callback(err)

    # Init bulk operation for mass update db
    bulk = Bot.db.items.initializeUnorderedBulkOp()
    
    for item in data.items
      do (item) ->
        # Re-mapping name of some properties
        renameProperty item, 'id', 'item_id'
        renameProperty item, 'localized_name', 'dname'
        item.name = item.name.replace 'item_', ''
        
        # Queue data into operation
        bulk.find { item_id: { $eq: item.item_id } }
          .upsert()
          .updateOne item
    
    # Execute db operation
    bulk.execute (err, result) ->
      if err
        callback(new Error(err.errmsg))
      callback null, result

### Helper functions ###
renameProperty = (object, old_key, new_key) ->
  if object.hasOwnProperty old_key
    object[new_key] = object[old_key]
    delete object[old_key]
  return

exports.convertAccountIDtoSteamID = (account_id) ->
  STEAM_ID_UPPER_32_BITS = '00000001000100000000000000000001'
  $hi = parseInt STEAM_ID_UPPER_32_BITS, 2
  $seed = 4294967296
  return bignum.add(bignum.mul($hi, $seed), account_id).toString()
