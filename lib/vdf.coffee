###
vdf.coffee
https://bitbucket.org/dzdev/dotabot

This file is part of dotabot
Copyright (c) 2015 DZ Team
Licensed under the LGPL-3.0 license.
###

# Convert VDF to JSON then parse as object
exports.parse = (vdf_string) ->
  ###
  A VDF string is typically like this (except the bracketed numbers):
"items_game" [1]
{
	"game_info"[1]
	{
		"first_valid_class"[2]		"1"[3]
		"last_valid_class"[2]		"1000"[3]
		"first_valid_item_slot"[2]		"0"[3]
		"last_valid_item_slot"[2]		"13"[3]
		"num_item_presets"[2]		"4"[3]
	}[4]
	"web_resources"[1]
	{
		"0"[1]
		{
			"name"[2]		"TI 2013 Results"[3]
			"url"[2]		"TI_2013_results.txt"[3]
			"on_demand"[2]		"0"
		}
	}
}
  ###
  # Add ":" to [1]
  vdf_string = vdf_string.replace /"\s*{/g, "\": {"
  # Add ":" to [2]
  vdf_string = vdf_string.replace /"\t\t/g, "\": "
  # Add "," to [3]
  vdf_string = vdf_string.replace /"(\s*)"/g, "\",$1\""
  # Add "," to [4]
  vdf_string = vdf_string.replace /}(\s*)"/g, "},$1\""
  # Wrap {}
  vdf_string = "{" + vdf_string + "}"
  
  # Return the object
  JSON.parse vdf_string
