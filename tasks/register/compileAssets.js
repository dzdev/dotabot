module.exports = function (grunt) {
	grunt.registerTask('compileAssets', [
		'clean',
		'less:dev',
		'copy:dev',
		'coffee:dev'
	]);
};
