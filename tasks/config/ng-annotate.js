/**
 * Add, remove and rebuild AngularJS dependency injection annotations.
 */
module.exports = function(grunt) {
  grunt.config.set('ngAnnotate', {
    options: {
      singleQuotes: true,
    },
    liveDota: {
      files: {
        '.tmp/public/concat/production.annotated.js': ['.tmp/public/concat/production.js']
      }
    }
  });
  grunt.loadNpmTasks('grunt-ng-annotate');
};
