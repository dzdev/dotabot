# dotabot

A bot for collecting data from Steam API

## Getting Started
Install the module with: `npm install dotabot`

Start the daemon
```shell
npm start
```

Start the server
```shell
node app
```

## Documentation

### Endpoint: /api/v1/

### Matches

### /live

Show a list of live matches. Get information for each match through /matches/*:match_id*/.

#### /matches

Show a list of completed matches. Get information for each match through /matches/*:match_id*/.

|Param|Type   |Default|Description|
|-----|-------|-------|-----------|
|page |int    | 1     | The page number to request|
|limit|int    | 10    | The number of items in each page. 0 is no limit|
|ids  |string | *none*| Comma-separated list of match ids to filter |

#### /matches/*:match_id*/

Show a match's information

#### /matches/*:match_id*/scoreboard

Show the scoreboard of a match

|Param         |Type   |Default|Description|
|--------------|-------|-------|-----------|
|from_duration |int    | 0     | Will only show scoreboard records no earlier than this duration |

### Players, heroes, items, teams, leagues

#### /*:collection*/

Show a list of collection items. `:collection` is one of: `players`, `heroes`, `items`, `teams` and `leagues`

|Param|Type   |Default|Description|
|-----|-------|-------|-----------|
|page |int    | 1     | The page number to request|
|limit|int    | 10    | The number of items in each page. 0 is no limit|
|ids  |string | *none*| Comma-separated list of item ids to filter |

#### /*:collection*/*:item_id*/

Show a collection item's information. `:item_id` value is taken from the key of each collection:

|Collection| Key       |
|----------|-----------|
|Players   | account_id|
|Heroes    | hero_id   |
|Items     | item_id   |
|Teams     | team_id   |
|Leagues   | league_id |

### Images

The image URL form for each of collection item:

|Item             | URL                                                     |
|-----------------|---------------------------------------------------------|
|Player Avatar    | /images/dota/players/player\_[:account\_id]\_small.png  |
|                 | /images/dota/players/player\_[:account\_id]\_medium.png |
|                 | /images/dota/players/player\_[:account\_id]\_full.png   |
|Team Logo        | /images/dota/teams/team_[:team_id].png                  |
|Team Sponsor Logo| /images/dota/teams/team_[:team_id]_sponsor.png          |
|Hero             | /images/dota/heroes/[:name]_full.png                    |
|                 | /images/dota/heroes/[:name]_hphover.png                 |
|                 | /images/dota/heroes/[:name]_sb.png                      |
|                 | /images/dota/heroes/[:name]_vert.jpg                    |
|Item             | /images/dota/items/[:name]_lg.png                       |

## License
Copyright (c) 2015 DZ Team  
Licensed under the LGPL-3.0 license.
